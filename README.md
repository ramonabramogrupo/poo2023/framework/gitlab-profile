![](ramon.jpeg)


# Programar un framework con el paradigma MVC

Hola, soy Ramon Abramo, tu profesor de programación en PHP. 

En este curso, te voy a enseñar cómo programar una aplicación en PHP utilizando el paradigma MVC (modelo vista controlador) y creando nuestro propio framework. ¿Qué es MVC y qué es un framework? Te lo explico a continuación:

Con MVC y un framework, podemos crear aplicaciones web más organizadas, escalables, mantenibles y seguras. En este curso, aprenderás los conceptos y las técnicas necesarias para crear tu propio framework con MVC y programar una aplicación con él. 

![logo](ramon.jpg)

Estoy seguro de que te resultará muy interesante y divertido. ¿Estás listo para empezar? 😊

## Introducción al MVC y la POO en PHP

MVC es un patrón de diseño que separa la lógica de la aplicación en tres componentes: 
- el modelo, que gestiona los datos y la interacción con la base de datos
- la vista, que muestra la información al usuario
- el controlador, que recibe las peticiones del usuario y las procesa usando el modelo y la vista .

![mvc](mvc.jpg)

El contenido general de esta parte:

- Explicar qué es el MVC, cómo se estructura y qué ventajas tiene para el desarrollo web¹².
- Explicar qué es la POO, cómo se define y manipula un objeto en PHP y qué beneficios aporta al código³⁴.
- Mostrar ejemplos de código de PHP con MVC y POO, usando clases, propiedades, métodos, herencia, polimorfismo, etc.

## Creación de un framework propio con MVC y POO en PHP

Un framework es un conjunto de herramientas, bibliotecas y convenciones que facilitan el desarrollo de aplicaciones web, proporcionando una estructura y una funcionalidad básica .

El contenido general de esta parte:

- Definir los requisitos y las funcionalidades del framework que se va a crear, así como el tipo de aplicación que se va a desarrollar con él.
- Diseñar la arquitectura del framework, estableciendo los directorios, archivos y convenciones de nombres que se van a seguir.
- Implementar el modelo, la vista y el controlador del framework, usando las bibliotecas y herramientas de PHP que se consideren necesarias.
- Probar y depurar el framework y la aplicación, usando técnicas y herramientas de testing⁵.
- Comparar el framework propio con otros frameworks PHP existentes, como Laravel, Symfony, CodeIgniter, etc⁶⁷. Analizar sus similitudes y diferencias, así como sus fortalezas y debilidades.